from flask import Flask, render_template, request, redirect, url_for
from flask_pymongo import pymongo
from bson.objectid import ObjectId


app = Flask(__name__)

CONNECTION_STRING = "mongodb+srv://mylene:root@cluster0-bakdz.gcp.mongodb.net/test?retryWrites=true&w=majority"
client = pymongo.MongoClient(CONNECTION_STRING)
db = client.get_database('todolist')
user_collection = pymongo.collection.Collection(db, 'todo')



#test to insert data to the data base
@app.route("/test")
def test():
    db.db.collection.insert_one({"name": "John"})
    return "Connected to the data base!"

# real URL

#home
@app.route("/")
def home():
	return render_template("welcome.html")

#ajouter un task in BDD
@app.route("/addTask", methods=['POST'])
def addTask():
	task = request.form["task"]
	db.db.collection.insert_one({"task": task})	
	return redirect('toDo')

#récuperer les task dans la bdd et les afficher
@app.route("/toDo", methods=['GET'])
def toDo():
	toDoList = db.db.collection.find({})
	result = []
	for task in toDoList:
		result.append({'_id': str(task['_id']),'task': task['task']})
	return render_template("welcome.html", result=result)

#suprr une task a partir de son id 
@app.route("/delTask", methods=['POST'])
def delTask():
	_id = request.form['key']
	db.db.collection.delete_one({'_id': ObjectId(_id)})
	return redirect('toDo')

if __name__ == '__main__':
    app.run(port=5000, debug=True)